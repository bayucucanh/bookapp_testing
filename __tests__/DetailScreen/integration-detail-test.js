import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { dataBooks } from './dataBooks'

const getDetailBook = async (token, id) => {
  try {
    const response = await axios
    .get(`http://code.aldipee.com/api/v1/books/${id}`, {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
    return response
  } catch (err) {
    return console.log(err)
  }
}

describe('Test get data books', () => {
  let mockApi
  beforeAll(() => {
    mockApi = new MockAdapter(axios);
  });

  afterEach(() => {
    mockApi.reset();
  });
  
  it('should get book data successfully', async () => {
    mockApi.onGet('http://code.aldipee.com/api/v1/books').reply(200, dataBooks)
    const results = await getDataBooks('token');
    expect(results.data).toEqual(dataBooks);
  })

  it('should get book data unsuccessfully', async () => {
    mockApi.onGet('http://code.aldipee.com/api/v1/books').reply(400, []);
    const result = await getDataBooks('token');
    console.log(result);
    expect(result).toEqual();
  });
})