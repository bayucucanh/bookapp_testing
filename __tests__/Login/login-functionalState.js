import React from 'react'
import { shallow, configure, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import renderer from 'react-test-renderer';

import {Login} from '../../src/Pages'

configure({ adapter: new Adapter(), disableLifecycleMethods: true });

jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));
jest.mock('react-native-push-notification', () => ({
  addEventListener: jest.fn(),
  requestPermissions: jest.fn(),
  then: jest.fn()
}));
jest.mock('react-native-share', () => ({
}));
jest.mock("react-native-video", () => "Video");
jest.mock("react-native-pdf", () => "Pdf");

const mockDispatch = jest.fn();

const loginWrapper = shallow(<Login />)

describe("Login Function and State test", () => {
  it('Input Form Login Exists', () => {
    expect(loginWrapper.find('Input[name="emailLogin"]').exists())
    expect(loginWrapper.find('Input[name="passwordLogin"]').exists())
    expect(loginWrapper.find('Input[name="submit"]').exists())
  });

  it('on submitting, a submit handler function should be triggered on click event', () => {
    expect(loginWrapper.find('Input[name="emailLogin"]').simulate('changeText', 'zunaedi@gmail.com'))
    expect(loginWrapper.find('Input[name="passwordLogin"]').simulate('changeText', 'zunaedi123'))
    expect(loginWrapper.find('Button[name="submit"]').simulate('press'))
    // const data = {
    //   email: 'zunaedi@gmail.com',
    //   password:'zunaedi123'
    // }
    // expect(loginWrapper.find('data').toEqual(data))
  });
})