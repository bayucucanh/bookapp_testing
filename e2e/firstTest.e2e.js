describe('Automation testing flow authentication', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  const goToBookApp = async () => {
    await expect(element(by.id('SplashId'))).toBeVisible();
    await waitFor(element(by.id('WelcomeId'))).toBeVisible().withTimeout(4000);
    await expect(element(by.id('WelcomeId'))).toBeVisible();
    await element(by.id('toBookapp')).tap();
  }

  const fillLogin = async () => {
    await element(by.id('email')).typeText('zunaedi@gmail.com');
    await element(by.id('password')).typeText('zunaedi123');
    await element(by.id('btnLogin')).tap();
  }

  const fillRegister = async () => {
    await element(by.id('setName')).typeText('bambang test9');
    await element(by.id('setEmail')).typeText('bambangtest9@gmail.com');
    await element(by.id('setPassword')).typeText('jonathan123');
    await element(by.id('register')).tap();
  }

  it('should login successfully', async () => {
    await goToBookApp()
    await fillLogin()
    await element(by.id('logout')).tap();
  });

  it('should register successfully', async () => {
    await goToBookApp()
    await element(by.id('toRegister')).tap();
    await fillRegister()
    await element(by.id('toLogin')).tap();
  });

  it('should register and login successfully', async () => {
    await goToBookApp()
    await element(by.id('toRegister')).tap();
    await element(by.id('setName')).typeText('bambang test19');
    await element(by.id('setEmail')).typeText('bambangtest19@gmail.com');
    await element(by.id('setPassword')).typeText('jonathan123');
    await element(by.id('register')).tap();
    await element(by.id('toLogin')).tap();
    await element(by.id('email')).typeText('bambangtest19@gmail.com');
    await element(by.id('password')).typeText('jonathan123');
    await element(by.id('btnLogin')).tap();
  });
});
