import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import { SECOND_COLOR } from '../../../utils/constant'

const Button = ({title, onPress, testID}) => {
  return (
    <View style={styles.wrapper.page}>
      <Text style={styles.wrapper.text.desc}>Silahkan masuk ke halaman {title}</Text>
      <TouchableOpacity style={styles.wrapper.btn} onPress={onPress} testID={testID}>
        <Text style={styles.wrapper.text.textBtn}>{title}</Text>
      </TouchableOpacity>
    </View>
  )
}

export default Button

const styles = StyleSheet.create({
  wrapper: {
    page: {
      marginVertical: 10,
      alignItems: 'center'     
    },
    btn: {
      width: '100%',
      height: 37,
      backgroundColor: SECOND_COLOR,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 5
    },
    text: {
      desc: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#fff'
      },
      textBtn: {
         color: '#fff',
         fontWeight: 'bold'
      }
    }
  },
})