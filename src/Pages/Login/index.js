import React, {useState} from 'react';
import {View, Button, TouchableOpacity, Text, Image, StatusBar} from 'react-native';
import {Input} from '../../Components';
import {LoggedIn} from '../../redux/';
import {useDispatch, useSelector} from 'react-redux';
import {PRIMARY_COLOR} from '../../utils/constant';
import {bgImageLogin} from '../../Assets';
import styles from './styles';

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();

  const sendData = () => {
    const data = {
      email: email,
      password: password,
    };
    console.log('form: ', data);
    return dispatch(LoggedIn(data, navigation));
  };

  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor={PRIMARY_COLOR}
      />
      {/* <LoginDraw style={styles.draw} width={250} height={250}/> */}
      <Image source={bgImageLogin} style={styles.imgBg} />

      <View style={styles.listInput}>
        <Text style={styles.title} testID='Login'>Login to your account</Text>
        <Input
          name="emailLogin"
          testID='email'
          placeholder="Email"
          value={email}
          onChangeText={text => setEmail(text)}
        />
        <Input
          testID='password'
          name='passwordLogin'
          placeholder="Password"
          secureTextEntry={true}
          value={password}
          onChangeText={text => setPassword(text)}
        />
      </View>
      <View style={{padding: 22, marginTop: -30}}>
        <Button title="Login" name="submit" color="#fd9210" onPress={sendData} testID='btnLogin'/>
        <Text style={{color: '#ffffff', textAlign: 'center', marginVertical: 17}}>
          Dont have an account ?
        </Text>
        <TouchableOpacity style={styles.btnRegister} onPress={() => navigation.navigate('Register')} testID='toRegister'>
          <Text style={{ color: '#fff', fontSize: 15, fontWeight: 'bold' }}>Register</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Login;

