import { StyleSheet, Text, View, Button } from 'react-native'
import React from 'react'
import Pdf from 'react-native-pdf';
import { PRIMARY_COLOR, PRIMARY_TEXT } from '../../utils/constant';

class PdfScreen extends React.Component {
  state = {
    pdfLink: '',
  };

  onButtonOpenClick = () => {
    this.setState({
      pdfLink: 'http://samples.leanpub.com/thereactnativebook-sample.pdf'
    })
  }

  render() {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.title}>Pdf Screen</Text>
      <Button title="Open PDF Document" onPress={this.onButtonOpenClick}/>
      {this.state.pdfLink ? (
          <Pdf
            source={{uri: this.state.pdfLink}}
            style={{width: 500, height: 500}}
          />
        ) : null}
    </View>
  )
  }
}

export default PdfScreen

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: PRIMARY_COLOR
  }, title: {
    color: '#fff', 
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 15,
  }
})