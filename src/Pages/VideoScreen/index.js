import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Video from 'react-native-video';
import { PRIMARY_COLOR } from '../../utils/constant';

const VideoScreen = () => {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.title}>My Video Project</Text>
      <Video
          source={{ uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4' }}
          style={{ width: 350, height: 300 }}
          fullscreen={false}
          controls={true}
          poster="https://m.media-amazon.com/images/M/MV5BOTI5ZTNkYWQtNDg2Mi00MTBmLTliMGItNTI5YWI5OTZkM2Y2XkEyXkFqcGdeQXVyNzU1NzE3NTg@._V1_QL75_UX500_CR0,47,500,281_.jpg"
          resizeMode='cover'
        />
    </View>
  )
}

export default VideoScreen

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: PRIMARY_COLOR
  }, title: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 15,
  }
})