import { StyleSheet, Text, View, Image} from 'react-native'
import React from 'react'
import { Button } from '../../Components/'
import { PRIMARY_COLOR } from '../../utils/constant'
import { welcomeUi } from '../../Assets'

const WelcomeAuth = ({navigation}) => {
  const handleGoTo = screen => {
    navigation.navigate(screen)
  }

  return (
    <View style={styles.wrapper} testID='WelcomeId'>
      <View style={styles.imgWelcome}>
        {/* <Image source={welcomeUi} style={styles.img}/> */}
      </View>
      <View style={styles.content}>
        <Text>Welcome Auth</Text>
        <Button testID='toBookapp' title='Book App' onPress={() => handleGoTo('Login')} />
        <Button title='Video Screen' style={styles.goTo} onPress={() => handleGoTo('VideoScreen')}/>
        <Button title='Pdf Screen' style={styles.goTo} onPress={() => handleGoTo('PdfScreen')}/>
      </View>
    </View>
  )
}

export default WelcomeAuth

const styles = StyleSheet.create({
  wrapper: {
    // alignItems: 'center',
    flex: 1,
    backgroundColor: PRIMARY_COLOR,
    paddingHorizontal: 20,
  },
  content: {
    justifyContent: 'center',
    flex: 1,
  },
  imgWelcome: {
    flex: 1,
    // backgroundColor: '#fff'
  },
  img: {
    width: '100%',
    height: '90%',
  },
})